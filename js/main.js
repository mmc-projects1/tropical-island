const burgerBtn = document.querySelector('.burger-btn')
const nav = document.querySelector('.nav')
const navItems = document.querySelectorAll('.nav__item')

console.log(burgerBtn)
console.log(nav)

function toggleNavigation() {
	nav.classList.toggle('nav--active')

	navItems.forEach(navItem => {
		navItem.addEventListener('click', () => {
			nav.classList.remove('nav--active')
		})
	})

	handleNavItemsAnimation()
}

function handleNavItemsAnimation() {
	let delayTime = 0

	navItems.forEach(navItem => {
		navItem.classList.toggle('nav-items-animation')
		navItem.style.animationDelay = '.' + delayTime + 's'
		delayTime++
	})
}

burgerBtn.addEventListener('click', toggleNavigation)
